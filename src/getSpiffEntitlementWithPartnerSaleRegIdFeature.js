import {inject} from 'aurelia-dependency-injection';
import ClaimSpiffServiceSdkConfig from './claimSpiffServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client'
import SpiffEntitlementWebView from './spiffEntitlementWebView';

@inject(ClaimSpiffServiceSdkConfig, HttpClient)
class GetSpiffEntitlementWithPartnerSaleRegIdFeature{

    _config:ClaimSpiffServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config:ClaimSpiffServiceSdkConfig,
                httpClient:HttpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;

    }

    /**
     * @param partnerSaleRegistrationId
     * @param accessToken
     * @returns {Promise|Promise.<SpiffEntitlementWebView>}
     */
    execute(partnerSaleRegistrationId:number,
            accessToken:string):Promise<SpiffEntitlementWebView[]> {

        return this._httpClient
            .createRequest(`claim-spiffs/regId/${partnerSaleRegistrationId}`)
            .asGet()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .send()
            .then(response => {
                return Array.from(
                    response.content,
                    contentItem =>
                        new SpiffEntitlementWebView(
                            contentItem.partnerRepUserId,
                            contentItem.spiffAmount
                        )
                );
            });
    }
}

export default GetSpiffEntitlementWithPartnerSaleRegIdFeature;
