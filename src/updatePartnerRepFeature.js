import {inject} from 'aurelia-dependency-injection';
import ClaimSpiffServiceSdkConfig from './claimSpiffServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client';

@inject(ClaimSpiffServiceSdkConfig, HttpClient)
class UpdatePartnerRepFeature {

    _config:ClaimSpiffServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config:ClaimSpiffServiceSdkConfig,
                httpClient:HttpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;
    }

    /**
     * Update the partner rep
     * @param {number} partnerSaleRegistrationId
     * @param {string} partnerRepUserId
     * @param {string} accessToken
     * @returns {Promise.<void>}
     */
    execute(partnerSaleRegistrationId:number,
            partnerRepUserId:string,
            accessToken:string):Promise<string> {

        return this._httpClient
            .createRequest(`claim-spiffs/entitlements/${partnerSaleRegistrationId}/partnerrepuserid/${partnerRepUserId}`)
            .asPost()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .send()
            .then(response => {
                return response;
            });
    }



}

export default UpdatePartnerRepFeature;