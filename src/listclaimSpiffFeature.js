import {inject} from 'aurelia-dependency-injection';
import ClaimSpiffServiceSdkConfig from './claimSpiffServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client'
import ClaimSpiffSynopsisView from './claimSpiffSynopsisView';

@inject(ClaimSpiffServiceSdkConfig, HttpClient)
class ListClaimSpiffFeature {

    _config:ClaimSpiffServiceSdkConfig;

    _httpClient:HttpClient;

    _cachedClaimSpiffs:Array<ClaimSpiffSynopsisView>;

    constructor(config:ClaimSpiffServiceSdkConfig,
                httpClient:HttpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;

    }

    /**
     * Lists claim spiffs with Ids
     * @param {string[]} claimIds
     * @param {string} accessToken
     * @returns {Promise.<ClaimSpiffSynopsisView[]>}
     */
    execute(claimIds:string[],accessToken:string):Promise<ClaimSpiffSynopsisView[]> {

        if (this._cachedClaimSpiffs) {

            return Promise.resolve(this._cachedClaimSpiffs);

        }
        else {

        }
        return this._httpClient
            .createRequest(`claim-spiffs/${claimIds.join()}`)
            .asGet()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .send()
            .catch(error => {
                console.log(error);
            })
            .then(response => {
                // cache
                this._cachedClaimSpiffs =
                    Array.from(
                        response.content,
                        contentItem =>
                            new ClaimSpiffSynopsisView(
                                contentItem.claimId,
                                contentItem.partnerSaleRegistrationId,
                                contentItem.partnerAccountId,
                                contentItem.partnerRepUserId,
                                contentItem.installDate,
                                contentItem.spiffAmount,
                                contentItem.spiffClaimedDate,
                                contentItem.facilityName,
                                contentItem.invoiceNumber
                            )
                    );

                return this._cachedClaimSpiffs;

            });
    }
}

export default ListClaimSpiffFeature;
