/**
 * @class {SpiffEntitlementWebView}
 */
export default class SpiffEntitlementWebView {

    _partnerRepUserId:string;

    _spiffAmount:number;

    /**
     * @param {number} partnerRepUserId
     * @param {number} spiffAmount
     */
    constructor(partnerRepUserId:string,
                spiffAmount:number
    ) {

        this._partnerRepUserId = partnerRepUserId;

        if (!spiffAmount) {
            throw new TypeError('spiffAmount required');
        }
        this._spiffAmount = spiffAmount;

    }

    /**
     * @returns {number}
     */
    get partnerRepUserId():string {
        return this._partnerRepUserId;
    }

    /**
     * @returns {number}
     */
    get spiffAmount():number {
        return this._spiffAmount;
    }

    toJSON() {
        return {
            partnerRepUserId:this._partnerRepUserId,
            spiffAmount: this._spiffAmount
        };
    }
}