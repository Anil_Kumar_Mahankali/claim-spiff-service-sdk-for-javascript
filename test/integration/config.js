import ClaimSpiffServiceSdkConfig from '../../src/claimSpiffServiceSdkConfig';

export default {
    claimSpiffServiceSdkConfig: new ClaimSpiffServiceSdkConfig(
        'https://api-dev.precorconnect.com'
    ),
    identityServiceJwtSigningKey: 'nbho9k9vcv8r48xGQs4woyN8BJ6q9X1efj295KXfS9A9yHJSRm0oU21j3ickrScQ',
    accountIdOfExistingAccountWithAnSapAccountNumber: '001K000001H2Km2IAF'
}
