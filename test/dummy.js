/**
 * dummy objects (see: http://xunitpatterns.com/Dummy%20Object.html)
 */
export default  {
    firstName:'firstName',
    lastName : 'lastName',
    claimId:1,
    partnerSaleRegistrationId: 1000001,
    partnerAccountId: '001A000001Fu80TIAR',
    partnerRepUserId:'00u5ih1oced5idOhc0h7',
    installDate:'11/24/2015',
    spiffAmount: 120,
    spiffClaimedDate:Date.now() + 10000 * 60,
    sapVendorNumber: '0000000000',
    facilityName:'Precor',
    invoiceNumber:'111111',
    url: 'https://dummy-url.com'

};