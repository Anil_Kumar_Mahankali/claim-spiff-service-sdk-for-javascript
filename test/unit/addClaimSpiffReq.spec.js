import AddClaimSpiffReq from '../../src/addClaimSpiffReq';
import dummy from '../dummy';

describe('AddClaimSpiffReq', ()=> {
    describe('constructor', () => {
        it('throws if partnerSaleRegistrationId is null', () => {
            /*
             arrange
             */
            const constructor =
                () => new AddClaimSpiffReq(
                    null,
                    dummy.partnerAccountId,
                    dummy.partnerRepUserId,
                    dummy.installDate,
                    dummy.spiffAmount,
                    dummy.spiffClaimedDate,
                    dummy.facilityName,
                    dummy.invoiceNumber
                );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'partnerSaleRegistrationId required');
        });
        it('get partnerSaleRegistrationId', () => {
            /*
             arrange
             */
            const expectedId = 1;

            /*
             act
             */
            const objectUnderTest =
                new AddClaimSpiffReq(
                    expectedId,
                    dummy.partnerAccountId,
                    dummy.partnerRepUserId,
                    dummy.installDate,
                    dummy.spiffAmount,
                    dummy.spiffClaimedDate,
                    dummy.facilityName,
                    dummy.invoiceNumber
                );

            /*
             assert
             */
            const actualId = objectUnderTest.partnerSaleRegistrationId;
            expect(actualId).toEqual(expectedId);

        });
        it('throws if partnerAccountId is null', () => {
            /*
             arrange
             */
            const constructor =
                () => new AddClaimSpiffReq(
                    dummy.partnerSaleRegistrationId,
                    null,
                    dummy.partnerRepUserId,
                    dummy.installDate,
                    dummy.spiffAmount,
                    dummy.spiffClaimedDate,
                    dummy.facilityName,
                    dummy.invoiceNumber
                );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'partnerAccountId required');
        });
        it('sets partnerAccountId', () => {
            /*
             arrange
             */
            const expectedId = "1";

            /*
             act
             */
            const objectUnderTest =
                new AddClaimSpiffReq(
                    dummy.partnerSaleRegistrationId,
                    expectedId,
                    dummy.partnerRepUserId,
                    dummy.installDate,
                    dummy.spiffAmount,
                    dummy.spiffClaimedDate,
                    dummy.facilityName,
                    dummy.invoiceNumber
                );

            /*
             assert
             */
            const actualId = objectUnderTest.partnerAccountId;
            expect(actualId).toEqual(expectedId);

        });
        it('throws if partnerRepUserId is null', () => {
            /*
             arrange
             */
            const constructor =
                () => new AddClaimSpiffReq(
                    dummy.partnerSaleRegistrationId,
                    dummy.partnerAccountId,
                    null,
                    dummy.installDate,
                    dummy.spiffAmount,
                    dummy.spiffClaimedDate,
                    dummy.facilityName,
                    dummy.invoiceNumber
                );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'partnerRepUserId required');
        });
        it('sets partnerRepUserId', () => {
            /*
             arrange
             */
            const expectedId = "12345";

            /*
             act
             */
            const objectUnderTest =
                new AddClaimSpiffReq(
                    dummy.partnerSaleRegistrationId,
                    dummy.partnerAccountId,
                    expectedId,
                    dummy.installDate,
                    dummy.spiffAmount,
                    dummy.spiffClaimedDate,
                    dummy.facilityName,
                    dummy.invoiceNumber
                );

            /*
             assert
             */
            const actualId = objectUnderTest.partnerRepUserId;
            expect(actualId).toEqual(expectedId);

        });
        it('throws if spiffAmount is null', () => {
            /*
             arrange
             */
            const constructor =
                () => new AddClaimSpiffReq(
                    dummy.partnerSaleRegistrationId,
                    dummy.partnerAccountId,
                    dummy.partnerRepUserId,
                    dummy.installDate,
                    null,
                    dummy.spiffClaimedDate,
                    dummy.facilityName,
                    dummy.invoiceNumber
                );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'spiffAmount required');
        });
        it('sets spiffAmount', () => {
            /*
             arrange
             */
            const expectedId = 1;

            /*
             act
             */
            const objectUnderTest =
                new AddClaimSpiffReq(
                    dummy.partnerSaleRegistrationId,
                    dummy.partnerAccountId,
                    dummy.partnerRepUserId,
                    dummy.installDate,
                    expectedId,
                    dummy.spiffClaimedDate,
                    dummy.facilityName,
                    dummy.invoiceNumber
                );

            /*
             assert
             */
            const actualId = objectUnderTest.spiffAmount;
            expect(actualId).toEqual(expectedId);

        });

        it('throws if facilityName is null', () => {
            /*
             arrange
             */
            const constructor =
                () => new AddClaimSpiffReq(
                    dummy.partnerSaleRegistrationId,
                    dummy.partnerAccountId,
                    dummy.partnerRepUserId,
                    dummy.installDate,
                    dummy.spiffAmount,
                    dummy.spiffClaimedDate,
                    null,
                    dummy.invoiceNumber
                );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'facilityName required');
        });
        it('sets facilityName', () => {
            /*
             arrange
             */
            const expectedId = "Precor";

            /*
             act
             */
            const objectUnderTest =
                new AddClaimSpiffReq(
                    dummy.partnerSaleRegistrationId,
                    dummy.partnerAccountId,
                    dummy.partnerRepUserId,
                    dummy.installDate,
                    dummy.spiffAmount,
                    dummy.spiffClaimedDate,
                    expectedId,
                    dummy.invoiceNumber
                );

            /*
             assert
             */
            const actualId = objectUnderTest.facilityName;
            expect(actualId).toEqual(expectedId);

        });
        it('throws if invoiceNumber is null', () => {
            /*
             arrange
             */
            const constructor =
                () => new AddClaimSpiffReq(
                    dummy.partnerSaleRegistrationId,
                    dummy.partnerAccountId,
                    dummy.partnerRepUserId,
                    dummy.installDate,
                    dummy.spiffAmount,
                    dummy.spiffClaimedDate,
                    dummy.facilityName,
                    null
                );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'invoiceNumber required');
        });
        it('sets invoiceNumber', () => {
            /*
             arrange
             */
            const expectedId = "1";

            /*
             act
             */
            const objectUnderTest =
                new AddClaimSpiffReq(
                    dummy.partnerSaleRegistrationId,
                    dummy.partnerAccountId,
                    dummy.partnerRepUserId,
                    dummy.installDate,
                    dummy.spiffAmount,
                    dummy.spiffClaimedDate,
                    dummy.facilityName,
                    expectedId
                );

            /*
             assert
             */
            const actualId = objectUnderTest.invoiceNumber;
            expect(actualId).toEqual(expectedId);

        });
    })
});
