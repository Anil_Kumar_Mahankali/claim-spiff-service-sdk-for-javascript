## Description
Precor Connect claim spiff service SDK for javascript.


## Setup

**install via jspm**  
```shell
jspm install claim-spiff-service-sdk=bitbucket:precorconnect/claim-spiff-service-sdk-for-javascript
```

**import & instantiate**
```javascript
import ClaimSpiffServiceSdk,{ClaimSpiffServiceSdkConfig} from 'claim-spiff-service-sdk'

const claimSpiffServiceSdkConfig = 
    new ClaimSpiffServiceSdkConfig(
        "https://api-dev.precorconnect.com"
    );
    
const claimSpiffServiceSdk = 
    new ClaimSpiffServiceSdk(
        claimSpiffServiceSdkConfig
    );
```

## Platform Support

This library can be used in the **browser**.

## Develop

#### Software
- git
- npm

#### Scripts

install dependencies (perform prior to running or testing locally)
```PowerShell
npm install
```

unit & integration test in multiple browsers/platforms
```PowerShell
# note: following environment variables must be present:
# SAUCE_USERNAME
# SAUCE_ACCESS_KEY
npm test
```